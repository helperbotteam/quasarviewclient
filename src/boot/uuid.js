import UUID from 'vue-uuid';

export default async ({ Vue }) => {
    Vue.use(UUID);
};