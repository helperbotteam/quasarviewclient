import VueNativeSock from "vue-native-websocket";

export default async ({ Vue }) => {
  Vue.use(VueNativeSock, `ws://${window.location.hostname}:${process.env.WEBSOCKET_PORT}/helperbot`, {
    format: "json",
    reconnection: true, // (Boolean) whether to reconnect automatically (false)
    reconnectionAttempts: 10, // (Number) number of reconnection attempts before giving up (Infinity),
    reconnectionDelay: 20 // (Number) how long to initially wait before attempting a new (1000)
  });
};
