import axios from "axios";

export default async ({ Vue }) => {
    Vue.prototype.$ia = {};

    Vue.prototype.$ia.train = async function() {
        const response = await axios.get(`${process.env.SERVER_API}/ia/train`);
        return response;
    }
}