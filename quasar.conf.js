// Configuration for your app

module.exports = function (ctx) {

  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    boot: ['axios', 'vueNativeSock', 'uuid', 'vuelidate', 'dbUtils', 'authUtils', 'IAUtils'],

    css: ['app.styl'],

    extras: [
      'roboto-font',
      'material-icons',
      'ionicons-v4',
      // 'mdi-v3',
      'fontawesome-v5',
      // 'eva-icons'
    ],

    framework: {
      // all: true, // --- includes everything; for dev only!

      lang: 'es',

      components: [
        'QLayout',
        'QHeader',
        'QDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QIcon',
        'QList',
        'QItem',
        'QItemSection',
        'QItemLabel',
        'QInput',
        'QRadio',
        'QCheckbox',
        'QImg',
        'QScrollArea',
        'QAvatar',
        'QCard',
        'QCardSection',
        'QCardActions',
        'QTabPanels',
        'QTabPanel',
        'QTabs',
        'QTab',
        'QRouteTab',
        'QSeparator',
        'QDialog',
        'QSelect',
        'QFooter',
        'QChatMessage',
        'QDate',
        'QPopupProxy',
        'QBtnGroup',
        'QSpinnerDots',
        'QCarousel',
        'QCarouselControl',
        'QCarouselSlide',
        'QFab',
        'QFabAction',
        'QMenu',
        'QBadge',
        'QInnerLoading',
        'QPopupEdit',
        'QTable',
        'QTh',
        'QTr',
        'QTd'
      ],

      directives: ['Ripple', 'ClosePopup'],

      // Quasar plugins
      plugins: ['Notify', 'Loading'],

      config: {
        notify: {
          timeout: 2000,
          color: 'red-10'
        },
      }

      // iconSet: 'ionicons-v4'
      // lang: 'de' // Quasar language
    },

    supportIE: false,

    build: {
      scopeHoisting: true,
      // vueRouterMode: 'history',
      vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack(cfg) {},
      env: ctx.dev ? {
        // so on dev we'll have
        WEBSOCKET_PORT: JSON.stringify('3002'),
        SERVER_API: JSON.stringify('http://localhost:3001')
      } : {
        // and on build (production):
        WEBSOCKET_PORT: JSON.stringify('3002'),
        SERVER_API: JSON.stringify('http://localhost:3001')
      },
    },

    devServer: {
      // https: true,
      // port: 8080,
      open: true, // opens browser window automatically
    },

    // animations: 'all' --- includes all animations
    animations: 'all',

    ssr: {
      pwa: false,
    },

    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {},
      manifest: {
        // name: 'Quasar App',
        // short_name: 'Quasar-PWA',
        // description: 'Best PWA App in town!',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [{
            src: 'statics/icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          },
        ],
      },
    },

    cordova: {
      // id: 'org.cordova.quasar.app'
    },

    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack(cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Window only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration
        // appId: 'quasar-app'
      },
    },
  };
};
