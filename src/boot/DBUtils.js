import axios from "axios";

export default async ({ Vue }) => {
    Vue.prototype.$db = {};

    Vue.prototype.$db.getStatus = async function(statusId) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/getStatus`, {
            data: {
                statusId: statusId
            }
          });

          return response.data.map((state) => {
              return { id: state.id, title: state.statusName };
          });
    }

    Vue.prototype.$db.getBotMessage = async function(idMessage) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/getBotMessage`, {
            data: {
                idMessage: idMessage
            }
          });

          return response.data;
    }

    Vue.prototype.$db.getUserChat = async function(userId) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/getUserChat`, {
            data: {
                userId: userId
            }
          });

          return response.data.map(message => {
            let agent = "bot"
            if (message.sender == 1) {
                agent = "user"
            } 
            return {
                id: message.id,
                agent: agent,
                message: [message.message],
                suggeretions: null
            }
          });
    }

    Vue.prototype.$db.getUserByMail = async function(userEmail) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/getUserByMail`, {
            data: {
                userEmail: userEmail
            }
          });

          return response.data;
    }

    Vue.prototype.$db.getUserStatus = async function(userId) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/getUserStatus`, {
            data: {
                userId: userId
            }
          });

          return response.data;
    }

    Vue.prototype.$db.getUnknownWords = async function() {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/getUnknownWords`);

        return response.data;
    }

    Vue.prototype.$db.getIntents = async function() {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/getIntents`);

        return response.data.map(({intent}) => {
            return intent;
        });
    }

    Vue.prototype.$db.registerUser = async function(userData) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/createUser`, {
            data: {
                userData: JSON.stringify(userData)
            }
        });
    }

    Vue.prototype.$db.checkUser = async function(username, password) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/checkUser`, {
            data: {
                username: username,
                password: password
              }
        });
        return await response.data
    }

    Vue.prototype.$db.checkSocialUser = async function(userEmail) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/checkSocialUser`, {
            data: {
              userEmail: userEmail
            }
          });
          return await response.data;
    }

    Vue.prototype.$db.insertUserStatus = async function(userId, statusId) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/insertUserStatus`, {
            data: {
                userId: userId,
                statusId: statusId
            }
          });
          return response.data;
    }

    Vue.prototype.$db.saveUserChat = async function(messageData) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/saveToChat`, {
            data: {
                messageData: JSON.stringify(messageData)
            }
          });
          return response.data;
    }
    
    Vue.prototype.$db.insertKnownWords = async function(word, intent) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/insertKnownWords`, {
            data: {
                word: word,
                intent: intent
            }
          });

          return response.data;
    }

    Vue.prototype.$db.removeUnknownWords = async function(id) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/removeUnknownWords`, {
            data: {
                id: id,
            }
          });

          return response.data;
    }

    Vue.prototype.$db.disableUser = async function(userId) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/disableUser`, {
            data: {
            userId: userId
            }
        });
    }

    Vue.prototype.$db.updateUser = async function(userData) {
        const response = await axios.post(`${process.env.SERVER_API}/bbdd/updateUser`, {
            data: {
              userData: JSON.stringify(userData)
            }
        });

    }
};