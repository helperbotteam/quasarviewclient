
const routes = [
  {
    path: '/',
    component: () => import('layouts/EmptyLayout.vue'),
    children: [
      { path: 'login', component: () => import('pages/Login.vue'), alias: '' },
      { path: 'register', component: () => import('pages/Register.vue') },
      {
        path: 'panel',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: 'chat', component: () => import('pages/Chat.vue'), alias: '' },
          { path: 'seguimiento', component: () => import('pages/Tracing.vue')},
          { path: 'perfil', component: () => import('pages/Profile.vue')},
        ]
      },
      {
        path: 'auditoria',
        component: () => import('layouts/AdminLayout.vue'),
        children: [
          { path: '', component: () => import('pages/TrainerIA.vue') }
        ]

      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
