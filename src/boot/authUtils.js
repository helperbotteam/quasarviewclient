import jwt_decode from 'jwt-decode';

export default async ({ Vue }) => {
    Vue.prototype.$auth = {};

    Vue.prototype.$auth.getInfoUserFromToken = function(token) {
        let decoded = jwt_decode(token);
        return decoded;
    }

    Vue.prototype.$auth.getEmailUserFromToken = function(token) {
        let decoded = jwt_decode(token);
        return decoded.profile.emails[0].value;
    }
    
    Vue.prototype.$auth.logOut = function() {
        localStorage.removeItem('token');
        window.location.href = `/#/login`;
    }
}